#ifndef HOUGH_PARAMS_HPP
#define HOUGH_PARAMS_HPP

#include <cmath>
// cm単位で計算する
const long int max_h = 1200; // 縦幅の最大値
const long int max_w = 1200; // 横幅の最大値

const long int angle_split_size = 2<<10; // 角度の分割数
const double angle_delta = M_PI / angle_split_size;


#endif
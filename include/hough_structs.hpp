#ifndef HOUGH_STRUCTS_HPP
#define HOUGH_STRUCTS_HPP

#include "hough_params.hpp"
#include <cmath>
#include <vector>
#include <algorithm>
#include <opencv2/opencv.hpp>
#include <set>
#include <random>
#include <omp.h>
#include <ros/ros.h>

using pdd = std::pair<double, double>;
template <class T1, class T2>constexpr std::pair<T1,T2> operator+(const std::pair<T1, T2>& x, const std::pair<T1, T2>& y){ return std::pair<T1,T2>(x.first + y.first, x.second + y.second); };
template <class T1, class T2>constexpr std::pair<T1,T2> operator-(const std::pair<T1, T2>& x, const std::pair<T1, T2>& y){ return std::pair<T1,T2>(x.first - y.first, x.second - y.second); };
template <class T>constexpr std::pair<T,T> operator*(const std::pair<T, T>& x, const T& y){ return std::pair<T,T>(x.first*y, x.second*y); };
template <class T>constexpr std::pair<T,T> operator*(const T& x,const std::pair<T, T>& y){ return std::pair<T,T>(y.first*x, y.second*x); };


template<class T>bool chmax(T &former, const T &b) { if (former<b) { former=b; return true; } return false; }
template<class T>bool chmin(T &former, const T &b) { if (b<former) { former=b; return true; } return false; }


struct union_find{
    std::vector<long int> par;
    long int par_siz;
    union_find(long int n) : par(n){
        par_siz = n;
        for(long int i=0;i<n;i++)par[i]=-1; //init
    }
    void clear(){
        par.clear();
        par_siz = 0;
    }

    void init(long int n){
        par.resize(n);
        for(long int i=0;i<n;i++)par[i]=-1;//init
    }
    long int root(long int x){
        if(par[x]<0)return x;
        else return par[x] = root(par[x]);
    }
    void unite(long int p1,long int p2){
        long int pa1 = root(p1);
        long int pa2 = root(p2);
        if(pa1==pa2)return;
        if(par[pa1]>par[pa2]){ // |pa1| < |pa2|
            par[pa2] += par[pa1];
            par[pa1] = pa2;
        }else{ // |pa1| >= |pa2|
            par[pa1] += par[pa2];
            par[pa2] = pa1;
        }
        par_siz--;
    }
    long int size(long int x){ return (-1)*par[root(x)]; }
    long int par_size(){return par_siz;}
    bool same(long int p1,long int p2){ return root(p1) == root(p2); }
};



struct Line{
    cv::Point p1, p2;  // (p1.x < p2.x)
    double angle = 0.0;
    Line(cv::Vec4i& pts){
        if(pts[0] < pts[2]){
            p1 = cv::Point(pts[0], pts[1]);
            p2 = cv::Point(pts[2], pts[3]);
        }else{
            p2 = cv::Point(pts[0], pts[1]);
            p1 = cv::Point(pts[2], pts[3]);
        }
        angle = std::atan((double)(p1.y-p2.y)/(double)(p1.x - p2.x));
    }
    Line(const cv::Point& p1_, const cv::Point& p2_, double angle_){
        p1 = p1_;
        p2 = p2_;
        angle = angle_;
    }

    Line add_angle(double angle_){  //角度について順序を定義
        Line ans = *this;
        ans.angle = angle + angle_;
        return ans;
    }
};
bool operator<(const Line& l1, const Line& l2){ return (l1.angle < l2.angle); }
bool operator==(const Line& l1, const Line& l2){ return (l1.p1==l2.p1) && (l1.p2==l2.p2) && (l1.angle==l2.angle); }


struct circle{
    pdd center;
    double r;
    circle(double _x=0.0, double _y =0.0, double _r=0.0){
        center.first = _x;
        center.second = _y;
        r = _r;
    }
};

class spatial_division_base{
public:
    using pdd = std::pair<double, double>;
    template<class T> using matrix = std::vector<std::vector<T>>;
    std::vector<pdd> points;  // 点群を管理
    matrix<long int> table; // 集合を分割した時の各集合に属するindexの配列

    double dist(pdd& p1, pdd& p2){  // 距離
        return std::sqrt(std::pow(p1.first-p2.first, 2.0) + std::pow(p1.second-p2.second, 2.0));
    };
    circle calc_circ(pdd p1, pdd p2, pdd p3){  // 3点から円を計算する.
        circle ans;
        double x1 = p1.first, x2 = p2.first, x3 = p3.first;
        double y1 = p1.second, y2 = p2.second, y3 = p3.second;
        ans.center.first = (x1*x1+y1*y1)*(y2-y3) + (x2*x2+y2*y2)*(y3-y1) + (x3*x3+y3*y3)*(y1-y2);
        ans.center.first /= ( 2*(x1*(y2-y3) - y1*(x2-x3) + x2*y3 - x3*y2) );
        ans.center.second = (x1*x1 + y1*y1)*(x3-x2) + (x2*x2+y2*y2)*(x1-x3) + (x3*x3 + y3*y3)*(x2-x1);
        ans.center.second /= ( 2*(x1*(y2-y3) - y1*(x2-x3) + x2*y3 - x3*y2) );
        ans.r = sqrt((ans.center.first-x1)*(ans.center.first-x1) + (ans.center.second-y1)*(ans.center.second-y1));
        return ans;
    }
    pdd calc_center(pdd a, pdd b, double R){ // 2点と半径から中心点を計算する.
        pdd O = std::make_pair(0.0, 0.0);
        if((a.first == b.first) && (a.second == b.second))return O;
        pdd mid = (a+b)*0.5; // 中点
        pdd vect = std::make_pair((b.second - a.second), (a.first-b.first));
        double tmp = pow(R, 2) - dist(a, b) /4.0;
        if(tmp < 0.0)return O; // error
        double lg = tmp;
        double magni = sqrtf64x(lg/dist(vect, O));
        pdd ans = mid + (magni * vect);
        pdd ans2 = mid - (magni * vect);
        if(dist(ans,O) < dist(ans2, O))return ans2;
        else return ans;
    }

    void set_points(std::vector<pdd>& points_) {  // 点群をセットする関数
        points.clear();
        std::copy(points_.begin(), points_.end(), std::back_inserter(points));
    }
    virtual void space_division(){};  // 空間を分割する関数
    virtual void space_division(double d1, double d2){}; // 空間を分割する関数
    virtual void space_division(int d1, int d2){}; // 空間を分割する関数
};

class division_from_angle : public spatial_division_base{
public:
    void space_division(double max_dist, double max_diff_angle) override{
        int num_of_sets = 0; // 分割した集合の数
        double angle_pre = 0.0;  // 前の角度
        bool angle_flag = false; // 角度で判定するか否か
        table.resize(0);
        for(long int i=0; i<points.size(); i++){
            if((i==0) || (dist(points[i], points[i-1]) >= max_dist)){ // 距離判定
                table.push_back(std::vector<long int>{i});
                num_of_sets += 1;
                angle_flag = false;
                continue;
            }
            if(angle_flag){ // 角度の変化量で判定
                double angle_now = std::atan2(points[i].second-points[i-1].second, points[i].first-points[i-1].first);
                if(std::abs(angle_now - angle_pre) < max_diff_angle){
                    table[num_of_sets-1].push_back(i);
                    angle_pre = angle_now;
                }else{
                    angle_flag = false;
                    num_of_sets += 1;
                    table.push_back(std::vector<long int>{i});
                }
            }else{
                angle_pre = std::atan2(points[i].second-points[i-1].second, points[i].first-points[i-1].first);
                angle_flag = true;
                table[num_of_sets-1].push_back(i);
            }
        }
        ROS_INFO("set size is %zu", table.size());
    }
};

class division_uniform : public spatial_division_base {
public:
    void space_division(int num_w, int num_h) override{
        pdd ll = points[0], ur = points[0];
        for(auto& tmp : points){
            chmin(ll.first, tmp.first);
            chmin(ll.second, tmp.second);
            chmax(ur.first, tmp.first);
            chmax(ur.second, tmp.second);
        }
        double delta_w = (ur.first - ll.first) / (double)(num_w-1);
        double delta_h = (ur.second - ll.second) / (double)(num_h-1);
        matrix<std::vector<long int>> table_tmp;
        table_tmp.resize(num_w, std::vector<std::vector<long int>>(num_h));
        std::set<std::pair<int, int>> ids;
        for(int i=0; i<points.size(); i++){
            int idx = std::floor((points[i].first-ll.first)/delta_w);
            int idy = std::floor((points[i].second-ll.second)/delta_h);
            table_tmp[idx][idy].push_back(i);
            ids.insert(std::make_pair(idx, idy));
        }
        table.resize(0);
        for(auto& tmp : ids){
            table.push_back(table_tmp[tmp.first][tmp.second]);
        }
    }
};


class detect_circle : public division_from_angle{
private:
    std::random_device seed_gen;
    std::mt19937 engine;

    double evaluate(circle& circ, int table_id, double eps){  // 評価関数. 円の点群との一致度を計算
        long siz = table[table_id].size();
        int counter = 0;
        for(int i=0; i<table[table_id].size(); i++){
            auto tmp = table[table_id][i];
            if(std::abs(dist(points[tmp], circ.center)-circ.r) > eps)continue;
            counter += 1;
        }
        return (double)counter/(double)siz;
    }

    std::vector<int> gen_3pts(int max_len){
        if(max_len==3)return std::vector<int>{0, 1, 2};
        std::vector<int> ans;
        while(true){
            int tmp = engine() % max_len;
            bool flg = true;
            for(int i=0; i<ans.size(); i++){
                if(ans[i]==tmp){
                    flg = false;
                    break;
                }
            }
            if(flg)ans.push_back(tmp);
            if(ans.size() == 3)return ans;
        }
    }

    std::vector<int> gen_2pts(int max_len){
        if(max_len==2)return std::vector<int>{0,1};
        std::vector<int> ans;
        while(true){
            int tmp = engine() % max_len;
            int tmp2 = engine() % max_len;
            if(tmp != tmp2)return std::vector<int>{tmp, tmp2};
        }
    }

    std::pair<circle, double> ransac_circle(int table_id, int repeat_time, double eps, double max_radius, double min_radius){  // ransacで円を計算する
        circle ans;
        double max_value = -1.0; // evaluateは必ず正の値になる
        auto& tmp_table = table[table_id];
        long long int siz = tmp_table.size();
        repeat_time = (int)std::min((long long int)repeat_time, siz*siz*siz);
        for(int i=0; i<repeat_time; i++) {
            std::vector<int> tmp_pts = gen_3pts((int)siz);
            circle tmp_circle = calc_circ(points[tmp_table[tmp_pts[0]]], points[tmp_table[tmp_pts[1]]], points[tmp_table[tmp_pts[2]]]);
            if((tmp_circle.r > max_radius) || (tmp_circle.r < min_radius))continue;
            if (chmax(max_value, evaluate(tmp_circle, table_id, eps)))ans = tmp_circle;
        }
        return std::make_pair(ans, max_value);
    }

    std::pair<circle, double> ransac_circle2(int table_id, double radius, int repeat_time, double eps){  // ransacで円を計算する
        circle ans;
        double max_value = -1.0; // evaluateは必ず正の値になる
        auto& tmp_table = table[table_id];
        long long int siz = tmp_table.size();
        repeat_time = (int)std::min((long long int)repeat_time, siz*siz);
        for(int i=0; i<repeat_time; i++) {
            std::vector<int> tmp_pts = gen_2pts((int)siz);
            circle tmp_circle;
            tmp_circle.center = calc_center(points[table[table_id][tmp_pts[0]]], points[table[table_id][tmp_pts[1]]], radius);
            tmp_circle.r = radius;
            if (chmax(max_value, evaluate(tmp_circle, table_id, eps))){
                ans = tmp_circle;
            }
        }
        return std::make_pair(ans, max_value);
    }
public:
    detect_circle(){
        engine = std::mt19937(seed_gen());
    }
    std::vector<circle> calc_circle2(int repeat_time, double radius, double eps, double min_eval, bool single_point=true){
        // single_point=trueなら最良の円のみを返す
        if(table.empty())ROS_INFO("table is empty");
        std::vector<std::pair<circle, double>> circles;
        std::vector<circle> ans;
        double max_score = -1.0;
        for(int i=0; i<table.size(); i++){
            if(table[i].size() < 3)continue;
            circles.push_back(ransac_circle2(i, radius, repeat_time, eps));
        }
        if(circles.empty())return ans;
        if(single_point){  // 最良の円のみを返す
            double max_score = circles[0].second;
            int max_id = 0;
            for(int i=0; i<circles.size(); i++){
                if(chmax(max_score, circles[i].second))max_id = i;
            }
            ans.push_back(circles[max_id].first);
        }else{
            for(auto& tmp: circles)if(tmp.second >= min_eval)ans.push_back(tmp.first);
        }
        return ans;
    }

    std::vector<circle> calc_circle(int repeat_time, double eps, double max_radius, double min_radius, double min_eval){
        if(table.empty())ROS_INFO("table is empty");
        std::vector<std::pair<circle, double>> circles;
        std::vector<circle> ans;
        for(int i=0; i<table.size(); i++){
            if(table[i].size() < 3)continue;
            circles.push_back(ransac_circle(i, repeat_time, eps, max_radius, min_radius));
        }
        if(circles.empty())return ans;
        for(auto& tmp: circles)if(tmp.second >= min_eval)ans.push_back(tmp.first);
        return ans;
    }
};
#endif
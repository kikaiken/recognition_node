//
// Created by emile on 2021/12/03.
//

#ifndef HOUGH_TRANSFORM_HOUGH_FUNCTIONS_HPP
#define HOUGH_TRANSFORM_HOUGH_FUNCTIONS_HPP

#include "hough_structs.hpp"
#include <opencv2/opencv.hpp>
#include <cmath>
#include <vector>
#include <algorithm>

std::vector<Line> remove_similar_lines(std::vector<Line> sorted_lines, double threshold_dist = 10.0, double threshold_angle = M_PI/180.0){
    // 参照にすると多分バグる. 中でvector書き換えてるので
    std::vector<Line> ans;
    if(sorted_lines.empty())return ans;
    for(long int i_ = 0; i_ < sorted_lines.size(); i_++){
        if(i_==(sorted_lines.size()-1)){
            ans.push_back(sorted_lines[i_]);
            continue;
        }
        auto tmp = sorted_lines[i_];
        auto itr = sorted_lines.begin() + i_+1;
        auto right_ = std::upper_bound(itr, sorted_lines.end(), tmp.add_angle(threshold_angle));
        Line right = Line(cv::Point (-1,-1), cv::Point (-1,-1), 0.0);
        if(right_ != sorted_lines.end())right = *right_;
        // [i_, right)の範囲を検索する.

        cv::Point p1=tmp.p1, p2=tmp.p2;
        double angle = tmp.angle;
        long int counter = 1;
        while (itr !=sorted_lines.end() && !((*itr) == right)){
            if((cv::norm((*itr).p1-tmp.p1) < threshold_dist) && (cv::norm((*itr).p2 - tmp.p2) < threshold_dist)){
                p1 += (*itr).p1;
                p2 += (*itr).p2;
                angle += (*itr).angle;
                counter += 1;
                itr = sorted_lines.erase(itr);
            }else{
                itr++;
            }
        }
        p1 /= (double)counter;
        p2 /= (double)counter;
        angle /= (double)counter;
        ans.emplace_back(Line(p1, p2, angle));
    }
    return ans;
}

cv::Point calc_corner_point(Line& l1, Line& l2){
    cv::Point ans;
    double a = std::tan(l1.angle), b = std::tan(l2.angle);
    ans.x = std::floor( (double)(a*(double)l1.p1.x - b*(double)l2.p1.x - (double)l1.p1.y + (double)l2.p1.y)/(a-b));
    ans.y = std::floor( ((double)(l1.p1.x-l2.p1.x)*a*b + a*(double)l2.p1.y- b*(double)l1.p1.y )/(a-b) );
    return ans;
}

std::vector<cv::Point> calc_target_corners(std::vector<Line>& lines, double target_angle, double threshold = M_PI/180.0){ // 直線群から交点を計算して出力.
    std::vector<cv::Point> ans;
    if(lines.empty())return ans;
    for(long int i=0; i<lines.size()-1; i++){
        for(long int j=i; j<lines.size(); j++){
            if(std::abs(std::abs(lines[i].angle - lines[j].angle)-target_angle) < threshold){
                ans.emplace_back(calc_corner_point(lines[i], lines[j]));
            }
        }
    }
    return ans;
}


std::pair<cv::Point, double> calc_corner(cv::Vec4i& p1, cv::Vec4i& p2){ // 直線同士の交点と交わる角度を計算
    double a = (double)(p1[3]-p1[1])/(double)(p1[2]-p1[0]);
    double b = (double)(p2[3]-p2[1])/(double)(p2[2]-p2[0]);
    cv::Point ans;
    ans.x = floor((double)(a*p1[0] - b*p2[0] + p2[1] - p1[1])/(a-b));
    ans.y = floor( ((double)(p1[0]-p2[0])*a*b + (a*(double)p2[1]-b*(double)p1[1]))/(a-b) );
    return std::make_pair(ans, abs(atan(a)-atan(b)));
}

std::vector<cv::Point> calc_corners(std::vector<cv::Vec4i>& points, double min_angle){ // 直線群から交点を計算して出力.
    std::vector<cv::Point> ans;
    for(long int i=0; i<points.size()-1; i++){
        for(long int j=i; j<points.size(); j++){
            auto tmp = calc_corner(points[i], points[j]);
            if(tmp.second > min_angle)ans.push_back(tmp.first);
        }
    }
    return ans;
}

std::vector<cv::Point> calc_target_corners(std::vector<cv::Vec4i>& points, double target_angle, double threshold = M_PI/180.0*0.5){ // 直線群から交点を計算して出力.
    std::vector<cv::Point> ans;
    if(points.empty())return ans;
    for(long int i=0; i<points.size()-1; i++){
        for(long int j=i; j<points.size(); j++){
            auto tmp = calc_corner(points[i], points[j]);
            if(std::abs(tmp.second - target_angle) < threshold)ans.push_back(tmp.first);
        }
    }
    return ans;
}

std::vector<std::pair<double, double>> read_csv(std::string file_path){
    std::vector<std::pair<double, double>> ans;
    std::string str_buf;
    std::ifstream ifs_csv_file(file_path);

    while (getline(ifs_csv_file, str_buf)) {
        std::istringstream i_stream(str_buf);
        std::string tmp, tmp2;
        getline(i_stream, tmp, ',');
        getline(i_stream, tmp2, ',');
        ans.emplace_back(std::make_pair(stod(tmp), stod(tmp2)));
    }
    return ans;
}
//
//void detect_lines_test(){
//    using namespace std;
//    string file_path = "/home/emile/Documents/hough_transform/sample.csv";
//    auto vec = read_csv(file_path);
//    double w_max=0.0, h_max = 0.0;
//    for(auto& tmp: vec){
//        chmax(w_max, tmp.second);
//        chmax(h_max, tmp.first);
//    }
//    auto image = cv::Mat((int) ceil(w_max), (int) ceil(h_max), CV_8UC1, cv::Scalar(0)); // モノクロ画像
//    cv::Mat_<unsigned char> img = cv::Mat_<unsigned char>(image); // モノクロ画像
//    for(auto& tmp: vec){
//        img[(int) floor(tmp.second)][(int) floor(tmp.first)] = 255;
//    }
//
//    cv::imshow("", img);
//    cv::waitKey(0);
//
//    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 9)); // 前処理
//    cv::Mat tmp_img;
//    cv::morphologyEx(img, tmp_img, cv::MORPH_CLOSE, kernel);
//    // モルフォロジー変換
//    cv::imshow("", tmp_img);
//    cv::waitKey(0);
//
//
//    std::vector<cv::Vec4i> tmp_lines;
//    // 入力画像，出力，距離分解能，角度分解能，閾値，線分の最小長さ，2点が同一線分上にあると見なす場合に許容される最大距離
//    cv::HoughLinesP(tmp_img, tmp_lines, 1, CV_PI / 180, 400, 100, 100);  // ハフ変換
//
//
//    std::vector<Line> lines;
//    for(auto& tmp: tmp_lines)lines.emplace_back(Line(tmp)); // 計算結果を直線群に変換
//    std::sort(lines.begin(), lines.end());
//
//    lines = remove_similar_lines(lines);
//    cout<<"line size (removed)"<<lines.size()<<endl;
//    std::vector<cv::Point> corners_removed = calc_target_corners(lines, M_PI_2);
//    cout<<"corner size (removed)"<<corners_removed.size()<<endl;
//
//
//    cv::Mat dst_img = img.clone(); // ここからは描画処理
//    for(auto& tmp : lines){
//        cv::line(dst_img, tmp.p1, tmp.p2, cv::Scalar(255), 1, CV_AA);
//    }
//    for(auto& corner: corners_removed){
//        cv::circle(dst_img, corner, 30, cv::Scalar(255), 1, CV_AA);
//    }
//    cv::imshow("", dst_img);
//    cv::waitKey(0);
//
//    // 似た直線の除去無し
//    dst_img = img.clone();
//    auto corners = calc_target_corners(tmp_lines, M_PI_2);  // cornerの計算
//    //    auto corners = calc_corners(tmp_lines, M_PI/6);  // cornerの計算
//    cout<<"line size"<<tmp_lines.size() << endl;
//    cout<<"corner size"<<corners.size()<<endl;
//
//    for(auto& l : tmp_lines){
//        cv::line(dst_img, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(255), 1, CV_AA);
//    }
//    for(auto& corner: corners){
//        cv::circle(dst_img, corner, 30, cv::Scalar(255), 1, CV_AA);
//    }
//    cv::imshow("", dst_img);
//    cv::waitKey(0);
//}
//
//
//void detect_circle_test(){
//    using namespace std;
//    string file_path = "/home/emile/Documents/hough_transform/sample2.csv";
//    auto vec = read_csv(file_path);
//    double w_max=0.0, h_max = 0.0;
//    for(auto& tmp: vec){
//        chmax(w_max, tmp.second);
//        chmax(h_max, tmp.first);
//    }
//    omp_set_num_threads(4);
//    std::cout << "The number of processors is "<< omp_get_num_procs() << std::endl;
//
//    detect_circle ransac;
//
//    auto image = cv::Mat((int) ceil(w_max), (int) ceil(h_max), CV_8UC1, cv::Scalar(0)); // モノクロ画像
//    cv::Mat_<unsigned char> img = cv::Mat_<unsigned char>(image); // モノクロ画像
//    for(auto& tmp: vec){
//        img[(int) floor(tmp.second)][(int) floor(tmp.first)] = 255;
//    }
//
//    cv::imshow("", img);
//    cv::waitKey(0);
//
//    cv::Mat tmp_img, dst_img = img.clone();
//    ransac.set_points(vec);
//    cout<<"test"<<endl;
//    auto circles = ransac.calc_circle(6000, 400.0, 3.0, 10.0, 1);
//    cout<<"circles size"<<circles.size()<<endl;
//
//    for(auto& tmp: circles){
//        cv::Point center(cv::saturate_cast<int>(tmp.center.first), cv::saturate_cast<int>(tmp.center.second));
//        cout<<"center "<<tmp.center.first<<", "<<tmp.center.second<<", "<<tmp.r<<endl;
//        int radius = cv::saturate_cast<int>(tmp.r);
//        cv::circle(dst_img, center, radius, cv::Scalar(255), 1);
//    }
//
//    cv::imshow("", dst_img);
//    cv::waitKey(0);
//}



#endif //HOUGH_TRANSFORM_HOUGH_FUNCTIONS_HPP

//
// Created by emile on 2021/12/03.
//
#include "hough_structs.hpp"
#include "hough_functions.hpp"
#include <iostream>
#include <string>
#include <utility>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <boost/bind.hpp>
#include <vector>
#include <algorithm>
#include <omp.h>
#include <ros/ros.h>
#include <chrono>

using pdd = std::pair<double, double>;
using namespace std;


tf2::Transform setTF(double x, double y, double yaw){
    tf2::Transform output;
    tf2::Vector3 position(x, y, 0);
    tf2::Quaternion orientation;
    orientation.setRPY(0, 0, yaw);

    output.setOrigin(position);
    output.setRotation(orientation);
    return output;
}

void convert_laser_scan_to_pdd(const sensor_msgs::LaserScan::ConstPtr& msg, std::vector<std::pair<double, double>>& vec){
    // 点群データを(x,y)の配列に変換する
    vec.resize(msg->ranges.size());
    // msg->angle_increment : rad, angleの変動幅を意味する
    // msg->angle_min : rad, msg->ranges[0]の角度を意味する
    double min_dist = 1.0;
    long int idx = 0;
    for(long int i=0; i<msg->ranges.size(); i++){
        if((msg->ranges[i] < msg->range_min) || (msg->ranges[i] > msg->range_max) || (msg->ranges[i] < min_dist)){  // 無効なデータ
            continue;
        }else{
            double angle = msg->angle_min + (msg->angle_increment)*(double)i;  // angle(rad)
            vec[idx] = std::make_pair((msg->ranges[i] * cos(angle)), (msg->ranges[i] * sin(angle)));
            idx += 1;
        }
    }
    if(vec.size() > idx)vec.erase(vec.begin()+idx, vec.end());
}

void detect_circle_handler2(detect_circle* ransac, const sensor_msgs::LaserScan::ConstPtr& msg, tf2_ros::TransformBroadcaster& ref){
    std::chrono::system_clock::time_point  start, end; // 型は auto で可
    start = std::chrono::system_clock::now(); // 計測開始時間
    using pdd = std::pair<double, double>;
    std::vector<pdd> points;  // 点群の入った配列
    convert_laser_scan_to_pdd(msg, points);
    ransac->set_points(points);
    ransac->space_division(0.2, 10.0);
    auto circles = ransac->calc_circle2(1000, 0.5, 0.01, 0.75);
    for(long int i=0;i<circles.size();i++){
        geometry_msgs::TransformStamped trans_msg;
        trans_msg.header.stamp = ros::Time::now();
        if(i < circles.size()){
            trans_msg.transform = tf2::toMsg(setTF(circles[i].center.first, circles[i].center.second, 0.0));
        }else{
            trans_msg.transform = tf2::toMsg(setTF(0.0, 0.0, 0.0));
        }
        trans_msg.header.frame_id = "/urg_link";
        trans_msg.child_frame_id = "centor" + to_string(i);
        ref.sendTransform(trans_msg);
        ROS_INFO("center %i is (%lf, %lf)", (int)i, circles[i].center.first, circles[i].center.second);
    }
    end = std::chrono::system_clock::now();  // 計測終了時間
    double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count(); //処理に要した時間をミリ秒に変換
    ROS_INFO("%f ms", elapsed);
}

void detect_circle_handler(detect_circle* ransac, const sensor_msgs::LaserScan::ConstPtr& msg, tf2_ros::TransformBroadcaster& ref){
    std::chrono::system_clock::time_point  start, end; // 型は auto で可
    start = std::chrono::system_clock::now(); // 計測開始時間
    using pdd = std::pair<double, double>;
    std::vector<pdd> points;  // 点群の入った配列
    convert_laser_scan_to_pdd(msg, points);
    ransac->set_points(points);
    ransac->space_division(0.3, 10.0);
    auto circles = ransac->calc_circle(2000, 0.01, 0.7, 0.2, 0.8);
    for(long int i=0;i<circles.size();i++){
        geometry_msgs::TransformStamped trans_msg;
        trans_msg.header.stamp = ros::Time::now();
        if(i < circles.size()){
            trans_msg.transform = tf2::toMsg(setTF(circles[i].center.first, circles[i].center.second, 0.0));
        }else{
            trans_msg.transform = tf2::toMsg(setTF(0.0, 0.0, 0.0));
        }
        trans_msg.header.frame_id = "/urg_link";
        trans_msg.child_frame_id = "centor" + to_string(i);
        ref.sendTransform(trans_msg);
        ROS_INFO("center %i is (%lf, %lf)", (int)i, circles[i].center.first, circles[i].center.second);
    }
    end = std::chrono::system_clock::now();  // 計測終了時間
    double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count(); //処理に要した時間をミリ秒に変換
    ROS_INFO("%f ms", elapsed);
}

int main(int argc, char **argv){
    ros::init(argc, argv, "get_center");
    ros::NodeHandle n;
    division_from_angle test;
    ros::Rate loop_rate(20.0);
    tf2_ros::TransformBroadcaster ref_broadcaster;
    tf::StampedTransform transform;  // base_linkとbase_scanの変換用
    tf::TransformListener listener;
    detect_circle ransac_circle;
    ros::Subscriber sub = n.subscribe<sensor_msgs::LaserScan>("/laser/scan", 1,
                                                              boost::bind(detect_circle_handler, &ransac_circle, _1, ref_broadcaster));
    while(ros::ok()){
        loop_rate.sleep();
        ros::spinOnce();
    }
    ros::spin();
    return 0;
}


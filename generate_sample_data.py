import numpy as np
import csv

a = 1
b = 1


def f(x):
    return a * x + b


def rotate(points, theta_, center_x, center_y):
    xx, yy = points[0] - center_x, points[1] - center_y
    points[0] = xx * np.cos(theta_) - yy * np.sin(theta_) + center_x
    points[1] = xx * np.sin(theta_) + yy * np.cos(theta_) + center_y
    return points


def gen_rect(w, h, x, y, theta, point_num):
    w1 = (np.random.rand(point_num, 1) - 0.5) * w + x
    w1 = np.concatenate([w1, np.ones(point_num).reshape(point_num, 1) * h * 0.5 + y], 1)

    w2 = (np.random.rand(point_num, 1) - 0.5) * w + x
    w2 = np.concatenate([w2, np.ones(point_num).reshape(point_num, 1) * (-h * 0.5) + y], 1)

    h1 = (np.random.rand(point_num, 1) - 0.5) * h + y
    h1 = np.concatenate([np.ones(point_num).reshape(point_num, 1) * w * 0.5 + x, h1], 1)

    h2 = (np.random.rand(point_num, 1) - 0.5) * h + y
    h2 = np.concatenate([np.ones(point_num).reshape(point_num, 1) * (-w * 0.5) + x, h2], 1)
    ans = np.concatenate([w1, w2, h1, h2], 0)
    for i in range(point_num * 4):
        ans[i] = rotate(ans[i], theta, x, y)
        ans[i][0] += np.random.normal(0, 2.0)
        ans[i][1] += np.random.normal(0, 2.0)
    return ans


def gen_circ(x, y, r, num):
    thetas = np.random.rand(num) * np.pi * 2.0
    ans = []
    for t in thetas:
        ans.append([x + r * np.cos(t) + np.random.normal(0, 1.0), y + r * np.sin(t) + np.random.normal(0, 1.0)])
    return ans


def gen_points(num, f_name):
    ans = []
    for i in range(num):
        tmp = np.random.rand() * 800
        y = f(tmp)
        x = tmp
        y += np.random.normal(0.0, 2.0)
        x += np.random.normal(0.0, 2.0)
        ans.append([x, y])

    with open(f_name, 'w') as fil:
        writer = csv.writer(fil)
        for tmp in ans:
            writer.writerow(tmp)


def write_points(points, f_name):
    with open(f_name, 'w') as fil:
        writer = csv.writer(fil)
        for tmp in points:
            writer.writerow(tmp)


if __name__ == '__main__':
    tmp = gen_rect(600, 800, 500, 500, np.pi / 6, 500)
    file_name = "/home/emile/Documents/hough_transform/sample.csv"
    # gen_points(1000, file_name)
    write_points(tmp, file_name)
    file_name = "/home/emile/Documents/hough_transform/sample2.csv"
    tmp = gen_circ(500, 600, 400, 1440)
    write_points(tmp, file_name)
